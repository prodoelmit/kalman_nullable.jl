# Filters using Nullable types to represent missing measurements

function update{T}(kf::KalmanFilter,y::Vector{Nullable{T}})
        return update(kf,Observation(y))
end

function update!{T}(kf::KalmanFilter,y::Vector{Nullable{T}})
        update!(kf,Observation(y))
        return kf
end

function predictupdate{T}(kf::BasicKalmanFilter,y::Vector{Nullable{T}})
    update(predict(kf),y)
end

function predictupdate!{T}(kf::BasicKalmanFilter,y::Vector{Nullable{T}})
    update!(predict!(kf),y)
end


